local Shape = {}

Shape.color = "red"
Shape.filled = true

function Shape:shape()
end

function Shape:shape(color,filled)
	Shape.color = color
	Shape.filled = filled
end
function Shape:getColor()
	return Shape.color
end
function Shape:setColor(color)
	Shape.color = color
end
function Shape:getFilled()
	return Shape.getFilled
end
function Shape:setFilled(filled)
	Shape.setFilled = filled
end
function Shape:toString()
	print(Shape.getFilled)
	print(Shape.getColor)
end

local CircleClass = {}

function CircleClass:Create()
	local Circle = {radius = 1.0}
	setmetatable(Circle,Shape)

	function Circle:circle()
	end
	function Circle:circle(radius)
		Circle.radius = radius
	end
	function Circle:circle(radius,color,filled)
		Circle.radius = radius
		Circle.color = color
		Circle.filled = filled
	end

	function Circle:getRadius()
		print(Circle.radius)
		return Circle.radius
	end
	function Circle:setRadius(radius)
		Circle.radius = radius
	end
	function Circle:getArea()
		return (3.14 * Circle.getRadius * Circle.getRadius)
	end
	function Circle:getPerimeter()
		return (2 * 3.14 * Circle.getRadius)
	end
	function Circle:toString()
		print(Circle.getRadius)
		print(Circle.getFilled)
		print(Circle.getColor)
	end
	return Circle
end

local RectangleClass = {}

function RectangleClass:Create()
	local Rectangle = {width = 1.0, length = 1.0}
	setmetatable(Rectangle,Shape)

	function Rectangle:rectangle()
	end

	function Rectangle:rectangle(width,length)
		Rectangle.width = width
		Rectangle.length = length
	end
	function Rectangle:rectangle(width,length,color,filled)
		Rectangle.width = width
		Rectangle.length = length
		Rectangle.color = color
		Rectangle.filled = filled
	end
	function Rectangle:getWidth()
		return Rectangle.getWidth
	end
	function Rectangle:setWidth(width)
		Rectangle.width = width
	end
	function Rectangle:getLength()
		return Rectangle.getLength
	end
	function Rectangle:setLength(length)
		Rectangle.length = length
	end

	function Rectangle:getArea()
		return (Rectangle.getWidth * Rectangle.getLength)
	end
	function Rectangle:getPerimeter()
		return (2 * (Rectangle.getLength + Rectangle.getWidth))
	end
	function Rectangle:toString()
		print(Rectangle.getWidth)
		print(Rectangle.getLength)
		print(Rectangle.getPerimeter)
		print(Rectangle.getArea)
	end

	return Rectangle
end

local SquareClass = {}

function SquareClass:Create()
	local Square  = {}
	setmetatable(Square,Rectangle)

	function Square:square()
	end
	function Square:square(side)
		Square.side = side
	end
	function Square:square(side,color,filled)
		Square.side = side
		Square.color = color
		Square.filled = filled
	end
	function Square:getSide()
		print(Square.side)
		return Square.getSide
	end
	function Square:setSide(side)
		Square.side = side
	end
	function Square:setWidth(width)
		Square.width = width
	end
	function Square:setLength(length)
		Square.length = length
	end
	function Square:toString()
		print(Square.getSide)
		print(Square.getLength)
		print(Square.getWidth)
	end

	return Square
end
Shape:shape("blue", false)

a1 = CircleClass.Create(1)
a1:circle(2.0,"baa",true)
a1:getRadius()

a2 = SquareClass.Create(1)
a2:square(14)
a2:getSide()